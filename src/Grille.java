import java.util.ArrayList;
import java.util.List;

/**
 * Classe gérant l'intégralité de la grille du Démineur.
 */
public class Grille extends ArrayList<List<Case>>{
    /**
     * Attribut permettant de connaître le nombre de lignes de la grille
     */
    private int nbLignes;

    /**
     * Attribut permettant de connaître le nombre de colonnes de la grille
     */
    private int nbColonnes;

    /**
     * Attribut permettant de connaître le nombre de bombe a disperser sur la grille
     */
    private int nbBombes;


    /**
     * Constructeur permettant de créer une grille de plusieurs cases.
     * On génère ensuite le nombre de bombes données de manière aléatoire dans la grille.
     * 
     * @param nbLignes
     * @param nbColonnes
     * @param nbBombes
     */
    public Grille(int nbLignes, int nbColonnes, int nbBombes){
        super();
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.nbBombes = nbBombes;

        // On veut ajouter les cases dans la grille (la liste de listes)
        for(int i=0; i<nbLignes; i++){
            // on créer chaque ligne séparément qu'on ajoute après
            List<Case> ligne = new ArrayList<Case>();
            for(int j=0; j<nbColonnes; j++){
                ligne.add(new Case(j,i));   
            }
            this.add(ligne);
        }
        // on génère les bombes dans la grille
        this.genererBombes();     
    }

    /**
     * Méthode réinitialisant la grille
     */
    public void init(){
        List<List<Case>> test = new ArrayList<>();
        for(List<Case> t : this){
            test.add(t);
        }
        for(List<Case> a : test){
            this.remove(a);
        }
        
        // On veut ajouter les cases dans la grille (la liste de listes)
        for(int i=0; i<nbLignes; i++){
            // on créer chaque ligne séparément qu'on ajoute après
            List<Case> ligne = new ArrayList<Case>();
            for(int j=0; j<nbColonnes; j++){
                ligne.add(j, new Case(j,i));   
            }
            this.add(ligne);
        }
        // on génère les bombes dans la grille
        this.genererBombes();
        
    }

    /**
     * Méthode permettant de connaître le nombre de lignes de la grille
     * 
     * @return le nombre de lignes de la ligne
     */
    public int getNbLignes(){
        return this.nbLignes;
    }

    /**
     * Méthode permettant de connaître le nombre de colonnes de la grille
     * 
     * @return le nombre de colonnes de la grille
     */
    public int getNbColonnes(){
        return this.nbColonnes;
    }
    
    /**
     * Méthode permettant de connaître le nombre de bombes sur la grille
     * 
     * @return le nombre de bombres sur la grille
     */
    public int getNbBombes(){
        return this.nbBombes;
    }

    /**
     * Méthode permettant de récupérer une case en fonction de ses coordonées dans la grille
     * 
     * @param l le numéro de ligne de la case dans la grille
     * @param c le numéro de colonne de la case dans le grille
     * 
     * @return la case se trouvant aux coordonnées (l, c)
     */
    public Case getCase(int l,int c){
        return this.get(l).get(c);
    }

    /**
     * Méthode permettant de trouvé combien de case ont été rélévées
     * 
     * @return le nombre de case révélées
     */

    public int getNombresDeCasesRevelee(){
        int casesRevelees = 0;
        for(int l=0; l<getNbLignes(); l++){
            for(int c=0; c<getNbColonnes(); c++){
                Case laCase = getCase(l, c);
                if(laCase.estRelevee()){
                    casesRevelees +=1;
                }
            }
        }
        return casesRevelees;
    }

    /**
     * Méthode permettant de trouvé combien de case ont été marquées
     * 
     * @return le nombre de case marquées
     */
    public int getNombresDeCasesMarquee(){
        int casesMarquees = 0;
        for(int l=0; l<getNbLignes(); l++){
            for(int c=0; c<getNbColonnes(); c++){
                Case laCase = getCase(l, c);
                if(laCase.estMarquee()){
                    casesMarquees +=1;
                }
            }
        }
        return casesMarquees;
    }

    /**
     * Méthode privée générant le nombre de bombres indiquées dans le constructeur sur le nombre
     * de cases dans la grille
     */
    private void genererBombes(){
        int bombePosees = 0;
        // permet de placer les bombes de manière aléatoire en suivant le nombre de bombes que l'on pose
        while(bombePosees < this.nbBombes){
            // on prend une case au hasard (avec ligne et colonne au hasard)
            int indLig = (int)(Math.random() * this.getNbLignes());
            int indCol = (int)(Math.random() * this.getNbColonnes());
            Case caseAMinee = this.getCase(indLig, indCol);
            // on vériie que la case n'est as déjà minée
            if(!caseAMinee.estBombe()){
                caseAMinee.ajouteBombe();
                ++bombePosees;
            }
        }
    }

    /**
     * Méthode permettant de révéler une case de la grille
     * 
     * @param numLig numéro de ligne de la case à révéler
     * @param numCol numéro de colonne de la case à révéler
     */
    public void revelerCase(int numLig, int numCol){
        if((numLig>=0 && numLig<=this.getNbLignes()-1) &&
           (numCol>=0 && numCol<=this.getNbColonnes()-1)){
            Case laCase = this.getCase(numLig, numCol);
            laCase.relever();
           }
    }

    /**
     * Méthode permettant de compter le nombre de voisines minées autour de la case en question
     * 
     * @param laCase la case dont on souhaite connaître le nombre de voisines minées
     * 
     * @return le nombre de bombes autour de la case
     */
    public int compteVoisinesMinees(Case laCase){
        // on récupère les coordonées de la case dans le grille
        int x = laCase.getCordx();
        int y = laCase.getCordy();
        int voisinesMinnes = 0;
        // on parcourt les cases directement adjacentes à la case en question
        for(int i=-1; i<2;++i){
            for(int j=-1; j<2; ++j){
                // on teste que les voisines existent dans la grille et qu'on ne regarde pas la case actuelle
                if((x+i>=0 && x+i<this.getNbColonnes()) &&
                   (y+j>=0 && y+j<this.getNbLignes()) &&
                   !(x+i==x && y+j==y)){
                    Case caseVoisine = this.getCase(y+j, x+i);
                    if(caseVoisine.estBombe())
                        voisinesMinnes += 1;
                }
            }
        }
        return voisinesMinnes;
    }

    /**
     * Méthode permettant d'afficher la grille avec les états actuels des cases
     */
    public void affiche(){
        // On parcourt toutes les case que l'on va devoir afficher
        for(int l=0; l<this.getNbLignes(); ++l){
            this.ligneADessiner();
            String colonne = "| ";
            for(int c=0; c<this.getNbColonnes(); ++c){
                Case caseAAfficher = this.getCase(l, c);
                String affichageCase = caseAAfficher.getAffichage();
                if(affichageCase.equals("v")){
                    affichageCase = String.valueOf(this.compteVoisinesMinees(caseAAfficher));
                }
                colonne += affichageCase + " | ";
            }
            System.out.println(colonne);
        }
        this.ligneADessiner();
    }

    /**
     * Méthode permettant de savoir si la partie est perdue.
     * On sait qu'une partie est perdue quand une case qui contient une bombe est révélée
     * @return si la partie est perdue
     */
    public boolean estPerdu(){
        // on va parcourir toutes les cases pour vérifier la condition de défaite
        for(int l=0; l<this.getNbLignes(); ++l){
            for(int c=0; c<this.getNbColonnes(); ++c){
                Case uneCase = this.getCase(l, c);
                if(uneCase.estBombe() && uneCase.estRelevee())
                    return true;
            }
        }
        // si on parcourt toutes les cases alors la partie n'est pas perdue
        return false;
    }

    /**
     * Méthode permettant de savoir si une partie est gagnée
     * @return si la partie est gagnée (toutes les cases ont été révélées sauf les bombes)
     */
    public boolean estGagne(){
        int nbTotalCases = this.getNbLignes() * this.getNbColonnes();
        // si toutes les cases sont révélées sauf les bombes
        return nbTotalCases - this.nbBombes == this.getNombresDeCasesRevelee();
    }

    /**
     * Méthode qui permet d'afficher la chaîne de caractère dessinant les bordures horizontales 
     * de la grille qui est utilisée dans le méthode affiche()
     */
    private void ligneADessiner(){
        String ligneDessinee = "+";
        for(int i=0; i<this.getNbColonnes(); ++i){
            ligneDessinee += "---" + "+";
        }
        System.out.println(ligneDessinee);
    }

    /**
     * Méthode permettant de montrer la grille avec les cases révélées
     */
    public void revelerTout(){
        for(int l=0; l<this.getNbLignes(); l++){
            for(int c=0; c<this.getNbColonnes(); c++){
                this.getCase(l, c).relever();
            }
        }
    }

}
