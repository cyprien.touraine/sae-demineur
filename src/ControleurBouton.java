import javafx.event.EventHandler; 
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.MouseButton;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.Optional;

public class ControleurBouton implements EventHandler<MouseEvent>{
    
    private Bouton bouton;
    private Case laCase;
    private DemineurGraphique demineur;
    private Grille lePlateau;
    
    public ControleurBouton(Bouton bouton, Case laCase, DemineurGraphique demineur, Grille lePlateau){
        this.bouton = bouton;
        this.laCase = laCase;
        this.demineur = demineur;
        this.lePlateau = lePlateau;
    }
    
    @Override
    public void handle(MouseEvent e) {
        if (!this.laCase.estRelevee()){
            if (e.getButton() == MouseButton.PRIMARY)
            {
                System.out.println("clic gauche");
                this.laCase.relever();
                this.bouton.maj();                
                this.bouton.setDisable(true);
            }
            
            if(e.getButton() == MouseButton.SECONDARY){
                System.out.println("clic droit");
                if (!this.laCase.estRelevee()){
                    this.laCase.marquer();
                    this.bouton.maj();
                }
            }
        }
        
        this.demineur.maj_des_infos();
        
        System.out.println(this.lePlateau.estPerdu());
        if (this.lePlateau.estPerdu()){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Vous avez perdu !\nVoulez-vous rejouer ?",ButtonType.YES, ButtonType.NO);
            alert.setTitle("Attention");
            Optional<ButtonType> rep = alert.showAndWait();
            
            if (rep.isPresent() && rep.get()==ButtonType.YES){
                this.lePlateau.init();
                this.demineur.maj_de_la_grille();
                this.demineur.maj_des_infos();
            }
            else{
                this.demineur.desactiver();
            }            
        }
    }
}
