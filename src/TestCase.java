import static org.junit.Assert.assertEquals;

import org.junit.*;


public class TestCase{

    private Case c1;
    private Case c2;
    private Case c3;
    private Case c4;
    private Case c5;

    @Before
    public void initialisation() {
        this.c5 = new Case(5, 5);
        this.c5.ajouteBombe();
        this.c5.relever();
        this.c4 = new Case(4, 4);
        this.c4.ajouteBombe();
        this.c4.marquer();
        this.c3 = new Case(3, 3);
        this.c3.ajouteBombe();
        this.c3.relever();
        this.c2 = new Case(2, 2);
        this.c2.ajouteBombe();
        this.c2.marquer();
        this.c1 = new Case(1, 1);
    }
    
    @Test
    public void test_getCordX() {
        assertEquals(c5.getCordx(),5);
        assertEquals(c4.getCordx(),4);
        assertEquals(c3.getCordx(),3);
        assertEquals(c2.getCordx(),2);
        assertEquals(c1.getCordx(),1);
    }

    @Test
    public void test_getCordy() {
        assertEquals(c5.getCordy(),5);
        assertEquals(c4.getCordy(),4);
        assertEquals(c3.getCordy(),3);
        assertEquals(c2.getCordy(),2);
        assertEquals(c1.getCordy(),1);
    }

    @Test
    public void test_estMinee() {
        assertEquals(c5.estBombe(),true);
        assertEquals(c4.estBombe(),true);
        assertEquals(c3.estBombe(),true);
        assertEquals(c2.estBombe(),true);
        assertEquals(c1.estBombe(),false);
    }

    @Test
    public void test_estMarquee() {
        assertEquals(c5.estMarquee(),false);
        assertEquals(c4.estMarquee(),true);
        assertEquals(c3.estMarquee(),false);
        assertEquals(c2.estMarquee(),true);
        assertEquals(c1.estMarquee(),false);
    }

    @Test
    public void test_estRevelee() {
        assertEquals(c5.estRelevee(),true);
        assertEquals(c4.estRelevee(),false);
        assertEquals(c3.estRelevee(),true);
        assertEquals(c2.estRelevee(),false);
        assertEquals(c1.estRelevee(),false);
    }

    @Test
    public void test_getAffichage() {
        assertEquals(c5.getAffichage(),"@");
        assertEquals(c4.getAffichage(),"?");
        assertEquals(c3.getAffichage(),"@");
        assertEquals(c2.getAffichage(),"?");
    }
}