import javafx.scene.control.Button; 
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class Bouton extends Button{
    
    private Case laCase;
    private Grille lePlateau;

    public Bouton(Case laCase, Grille lePlateau){
        super();
        this.setPrefWidth(30);
        this.setPrefHeight(30);
        this.laCase = laCase;
        this.lePlateau = lePlateau;
        if (laCase.estRelevee() && laCase.estBombe())
            ajouteImage("bombe.jpeg");
        else if (laCase.estRelevee() && laCase.estBombe())
            ajouteImage("bombe.jpeg");
        
    }
    
    private void ajouteImage(String fichierImage){
        try{
                Image image = new Image(fichierImage);
                ImageView iv = new ImageView();
                iv.setImage(image);
                iv.setFitWidth(20);
                iv.setPreserveRatio(true);
                this.setGraphic(iv);
            }
        catch(Exception e){
            this.setText(laCase.getAffichage());
        }
    }
    
    public void maj(){
        this.setText(this.laCase.getAffichage());
        if (this.laCase.estRelevee())
            this.setDisable(true);
        else
            this.setDisable(false);
        String affichage = this.laCase.getAffichage();
        System.out.println(affichage);
        if(affichage.equals("v"))
            affichage = String.valueOf(this.lePlateau.compteVoisinesMinees(this.laCase));
        System.out.println(affichage);
        this.setText(affichage);
    }
}
