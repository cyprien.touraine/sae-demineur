import static org.junit.Assert.assertEquals;

import org.junit.*;


public class TestGrille{

    private Grille g1;
    private Grille g2;
    private Grille g3;
    private Grille g4;
    private Grille g5;

    @Before
    public void initialisation() {
        this.g5 = new Grille(5, 5, 5);
        this.g4 = new Grille(4, 4, 4);
        this.g3 = new Grille(3, 3, 3);
        this.g2 = new Grille(2, 2, 2);
        this.g1 = new Grille(1, 1, 1);
    }
    
    @Test
    public void test_getNbLignes() {
        assertEquals(g5.getNbLignes(),5);
        assertEquals(g4.getNbLignes(),4);
        assertEquals(g3.getNbLignes(),3);
        assertEquals(g2.getNbLignes(),2);
        assertEquals(g1.getNbLignes(),1);
    }

    @Test
    public void test_getNbcolonne() {
        assertEquals(g5.getNbColonnes(),5);
        assertEquals(g4.getNbColonnes(),4);
        assertEquals(g3.getNbColonnes(),3);
        assertEquals(g2.getNbColonnes(),2);
        assertEquals(g1.getNbColonnes(),1);
    }

    @Test
    public void test_getNbBombe() {
        assertEquals(g5.getNbBombes(),5);
        assertEquals(g4.getNbBombes(),4);
        assertEquals(g3.getNbBombes(),3);
        assertEquals(g2.getNbBombes(),2);
        assertEquals(g1.getNbBombes(),1);
    }

}