/**
 * Classe qui permet de lancer le jeu du démineur.
 */
public class ExecutableDemineur {
    public static void main(String[] args) {
        Demineur partie = new Demineur();
        partie.lancerPartie();
    }
}
