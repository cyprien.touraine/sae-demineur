/**
 * Classe qui permet de gérer une case de la grille du démineur.
 * Elle permet de gérer les états de la case, c'est-à-dire si elle a été révelée ou marquée par l'utilisateur
 * ou encore de savoir si elle contient une bombe.
 */
public class Case{

    /**
     * Attribut qui permet de savoir si la case contient une bombe
     */
    private boolean estMinee;

    /**
     * Attribut permettant de savoir si la case a été découverte
     */
    private boolean estRelevee;

    /**
     * Attribut permettant de savoir si la case est marquée par un drapeau
     */
    private boolean estMarquee;

    /**
     * Attribut permettant de connaitre le numéro de ligne de la case dans la grille
     */
    private int cordx;

    /**
     * Attribut permettant de connaîre le numéro de colonne de la case dans la grille
     */
    private int cordy;


    /**
     * Constructeur permettant de créer une case. Par défaut elle n'est ni minée, ni révélée, ni marquée.
     * 
     * @param cordx Paramètre renseignant le numéro de ligne de la case dans la grille
     * @param cordy Paramètre renseignant le numéro de colonne de la case dans la grille
     */
    public Case(int cordx, int cordy){
        this.estMinee = false;
        this.estRelevee = false;
        this.estMarquee = false;
        this.cordx = cordx;
        this.cordy = cordy;
    }

    /**
     * Méthode permettant d'avoir un accès à la coordonée x de la case dans la grille
     * 
     * @return le numéro de la ligne de la case dans la grille
     */
    public int getCordx(){
        return cordx;
    }

    /**
     * Méthode permettant d'avoir un accès à la coordonée y de la case dans la grille
     * 
     * @return le numéro de  la colonne de la case dans la grille
     */
    public int getCordy(){
        return cordy;
    }

    /**
     * Méthode qui permet de faire de la case une case contenant une bombe
     */
    public void ajouteBombe(){
        this.estMinee = true; 
    }

    /**
     * Méthode permettant de savoir si la case est une case minée, c'est-à-dire contenant une bombe
     * 
     * @return L'état booléen de la case indiquant si la case contient une bombe
     */
    public boolean estBombe(){
        return this.estMinee;
    }

    /**
     * Méthode permettant de savoir si la case a été révélée
     * 
     * @return L'état booléen de la case indiquant si la case est révélée ou pas
     */
    public boolean estRelevee(){
        return this.estRelevee;
    }

    /**
     * Méthode permettant de faire de la case une case qui a été révélée
     */
    public void relever(){
        this.estRelevee = true;
    }

    /**
     * Méthode qui revoie si la case a été marquée, c'est-à-dire si un drapeau est placé dessus
     * 
     * @return L'état booléen de si la case est marquée ou non
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }

    /**
     * Méthode permettant de faire de la case une case qui a été marquée
     */
    public void marquer(){
        this.estMarquee = true;
    }

    /**
     * Méthode retourant l'affichage d'une case
     * " " si la case n’est ni révélée ni marquée
     * "@" si la case est révélée et contient une bombe
     * "?" si la case a été marquée
     * "v" si on doit compter le nombre de bombes autour des voisines
     * 
     * @return L'affichage de la case en fonction de ce qu'elle contient
     */
    public String getAffichage(){
        if(this.estRelevee()){
            if(this.estBombe()){
                // si la case est à la fois révélée et qu'elle est minée
                return "@";
            }else{
                // on retourne cela pour compter le nombre de voisins autour dans la classe Grille
                return "v";
            }
        }else{
            if(this.estMarquee()){
                // si elle n'est pas révélée mais que la case a été marquée
                return "?";
            }else{
                // si elle n'est pas ni révélée ni marquée
                return " ";
            }
        }        
    }
}
