import java.util.Scanner;

/**
 * Classe permettant d'implémenter le jeu du démineur à l'aide des autres classes (Grille et Case)
 */
public class Demineur {

    /**
     * Méthode permettant de jouer une partie jusqu'à la condition de défaite
     */
    public void lancerPartie(){
        // on créer une nouvelle grille d'une taille qu'on peut faire varier
        Grille grilleDeJeu = new Grille(3, 3, 1);
        grilleDeJeu.affiche();
        Scanner saisie = new Scanner(System.in);
        while(!grilleDeJeu.estPerdu() && !grilleDeJeu.estGagne()){
            // on récupère la saisie de l'utilisateur (ligne et colonne dont il souhaite découvrir la case)
            int numLig = this.demanderEntier(saisie, "Entrez le numéro de ligne", 1, grilleDeJeu.getNbLignes());
            int numCol = this.demanderEntier(saisie, "Entrez le numéro de colonne", 1, grilleDeJeu.getNbColonnes());
            // on fait -1 car on travaille avec les indices
            grilleDeJeu.revelerCase(numLig-1, numCol-1);
            grilleDeJeu.affiche();
        }
        if(grilleDeJeu.estGagne()){
            System.out.println("Félicitation ! Vous avez déminée le terrain !");
        }else{
            System.out.println("Dommage ! Vous êtes tombés sur une mine ! Try again !");
        }
        // quand la partie est terminée, on montre la grille entièrement révélée
        grilleDeJeu.revelerTout();
        grilleDeJeu.affiche();
        // on ferme le scanner
        saisie.close();
    }

    /**
     * Méthode permettant de récupérer la saisie d'un nombre entier d'un utilisateur
     * 
     * @param saisie le scanner permettant de récupérer la saisie textuelle de l'utilisateur
     * @param message message que l'on souhaite afficher à l'utilisateur avant sa saisie
     * @param valMin valeur minimale comprise que l'utilisateur peut saisir
     * @param valMax valeur maximale comprise que l'utilisateur peut saisir
     * 
     * @return l'entier saisi par l'utilisateur compris dans [valMin:valMax]
     */
    private int demanderEntier(Scanner saisie, String message, int valMin, int valMax){
        int valUser = valMin-1;
        boolean bonneValeur = false;
        // tant que l'utilisateur ne saisit pas une bonne valeur
        while(!bonneValeur){
            System.out.print(message+" ["+valMin+":"+valMax+"] : ");
            // on récupère la saisie de l'utilisateur
            valUser = saisie.nextInt();
            // on vérifie que la valeur est bien comprise entre les bornes
            if(valUser < valMin || valUser > valMax){
                System.out.println(valUser+" n'est pas compris entre "+valMin+" et "+valMax+" inclus.");
            }else{
                bonneValeur = true;
            }
        }
        return valUser;
    }
}